var express = require('express');
var router = express.Router();

const axios = require('axios');

let film = [{
    name: "Arthur",
    id: "0"
  }];
  

axios.get((req, res) => {
    res.status(200).json({ films });
    // REQ: Object de requete,
    // RES: Object de reponse.
    });
  
/* GET one user. */
axios.get('http://www.omdbapi.com/?i=tt3896198&apikey=c5ff372f'+'/:id', (req, res) => {
  const { id } = req.params;
  // Find user in DB
  const name = _.find(films, ["id", id]);
  // Return user
  res.status(200).json({
    message: 'Name found!',
    name 
  });
});

axios.put((req, res) => {
    const { name } = req.body;
    const id = _.uniqueId();
    films.push({ name, id });
    res.json({
        message: `Just added ${id}`,
        film: { name, id }
      });
    
    });

axios.post('/:id',(req, res) =>{
  const { id } = req.params;
  const { name } = req.body;
  const nameToUpdate = _.find(film, ["id", id]);
  nameToUpdate.name = name;
  res.json({
    message: `Just updated ${id} with ${name}`
  });

});

axios.delete('/:id',(req, res) =>{
  const { id } = req.params;
  _.remove(film, ["id", id]);
  res.json({
    message: `Just removed ${id}`
  });

});