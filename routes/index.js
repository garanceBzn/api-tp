var express = require('express');
var router = express.Router();

const hostname='localhost';
const port = 3000;

const app = express();

app.listen(port, hostname, () => {
  console.log('Mon serveur fonctionne sur http://${hostname}:${port} ');
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
